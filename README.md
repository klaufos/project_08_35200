# Pizza Bombelo

## Informacje o aplikacji
----------------------------------------------------------------------------------------
### Pizzeria Bombelo to aplikacja do zamawiania pizz i napoi.
### Po uruchomieniu aplikacji wyświetla się menu wejścia lub wyjścia.
### Następnie użytkownik zostaje przekierowany do ekranu wyboru (zestawy,pizza,napoje).
### Po wybraniu jednej z opcji program wyświetli produkty z danej kategorii.
### Kolejnym krokiem będzie wybranie co chcemy kupić i podanie ilości produktu.
### Program doda do koszyka produkt, wyświetli aktualną cene zamówienia i zapyta się klienta czy chce kontynuować zakupy.
### Jeśli klient odpowie twierdząco to program przekieruje go do ekranu wyboru.
### Jeśli klient chce zakończyć zamówienie wyswietli się rzeczywista cena produktu, aktywowane promocje i aktualna cena po obniżkach.
### Po zapłaceniu program zapyta się klienta czy chce paragon.
### Jeśli klient odmówi to program zakończy swoje działanie i podziękuje za zamówienie.
### Jeśli klient chce paragon, program wyswietli go i podsumuje zamówienie.

## Screeny aplikacji
----------------------------------------------------------------------------------------
![Screenshot](img/1.PNG)
![Screenshot](img/2.PNG)
![Screenshot](img/3.PNG)
![Screenshot](img/4.PNG)
![Screenshot](img/5.PNG)
![Screenshot](img/6.PNG)
![Screenshot](img/7.PNG)
![Screenshot](img/8.PNG)
![Screenshot](img/9.PNG)
![Screenshot](img/10.PNG)
![Screenshot](img/11.PNG)
![Screenshot](img/12.PNG)
![Screenshot](img/13.PNG)

## Narzędzia do tworzenia
----------------------------------------------------------------------------------------
[O języku C++](https://pl.wikipedia.org/wiki/C%2B%2B(język_programowania))

[Bitbucket](https://pl.wikipedia.org/wiki/Bitbucket)

[Qt Creator](https://pl.wikipedia.org/wiki/Qt_Creator)

[Git](https://pl.wikipedia.org/wiki/Git_(oprogramowanie))

## Podział prac
----------------------------------------------------------------------------------------
### Klaudia Foszcz (menu,funkcja płatności,funkcja wyboru,dane paragonu,podsumowanie)
### Jakub Chamielec (funkcja paragonu,funkcja pizz,zestawy,napoje)

